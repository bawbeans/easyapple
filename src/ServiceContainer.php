<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace EasyApple;

use Pimple\Container;

/**
 * Class ServiceContainer.
 */
class ServiceContainer extends Container
{
    /**
     * @var array
     */
    protected array $providers = [];

    /**
     * @var array
     */
    protected array $defaultConfig = [];
    /**
     * @var array
     */
    protected array $userConfig = [];

    public function __construct(array $config = [], array $prepends = [])
    {
        $this->userConfig = $config;

        parent::__construct($prepends);

        $this->registerProviders($this->getProviders());
    }

    /**
     * @return array
     */
    public function getConfig(): array
    {
        $base = [];

        return array_replace_recursive($base, $this->defaultConfig, $this->userConfig);
    }

    /**
     * Magic get access.
     * @param string $id
     * @return mixed
     */
    public function __get($id)
    {
        return $this->offsetGet($id);
    }


    /**
     * Magic set access.
     * @param string $id
     * @param mixed $value
     */
    public function __set($id, $value)
    {
        $this->offsetSet($id, $value);
    }

    /**
     * Return all providers.
     *
     * @return array
     */
    public function getProviders()
    {
        return array_merge([], $this->providers);
    }

    /**
     * @param array $providers
     */
    public function registerProviders(array $providers)
    {
        foreach ($providers as $provider) {
            parent::register(new $provider());
        }
    }
}
