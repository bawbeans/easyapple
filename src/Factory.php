<?php

namespace EasyApple;

/**
 * @method static Server\Application Server(array $config)
 * @method static ServerNotifications\Application ServerNotifications(array $config)
 * @method static Connect\Application connect(array $config)
 */
class Factory
{
    /**
     * @param string $name
     * @param array $config
     * @return ServiceContainer;
     */
    public static function make($name, array $config)
    {
        $value = ucwords(str_replace(['-', '_'], ' ', $name));
        $namespace = str_replace(' ', '', $value);
        $application = "EasyApple\\{$namespace}\\Application";

        return new $application($config);
    }

    /**
     * Dynamically pass methods to the application.
     *
     * @param string $name
     * @param array $arguments
     *
     * @return mixed
     */
    public static function __callStatic($name, $arguments)
    {
        return self::make($name, ...$arguments);
    }
}
