<?php

namespace EasyApple\ServerNotifications;

use EasyApple\ServiceContainer;

/**
 * Class Application.
 *
 * @property Notifications\Client $notifications
 */
class Application extends ServiceContainer
{
    /**
     * @var array
     */
    protected array $providers = [
        Notifications\ServiceProvider::class
    ];
}
