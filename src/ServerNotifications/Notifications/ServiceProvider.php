<?php

namespace EasyApple\ServerNotifications\Notifications;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * Class ServiceProvider.
 * @property Client $notifications
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['notifications'] = function ($app) {
            $config = $app->getConfig();
            return new Client($config);
        };
    }
}
