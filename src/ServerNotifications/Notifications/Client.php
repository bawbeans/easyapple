<?php

namespace EasyApple\ServerNotifications\Notifications;

use Firebase\JWT\JWT;

/**
 * Class Client.
 *
 */
class Client
{
    public array $config = [];

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function push($callback)
    {
        call_user_func($callback);
    }
}
