<?php

namespace EasyApple;

class Http
{
    protected $curl;

    protected string $type = 'GET';

    protected int $timeout = 10;

    protected array $option = [];

    protected string $response = '';

    protected int $statusCode;

    protected string $url;

    private function __construct()
    {
        $this->curl = curl_init();
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false); // 对认证证书来源的检查
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYHOST, false); // 从证书中检查SSL加密算法是否存在
        curl_setopt($this->curl, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0)'); // 模拟用户使用的浏览器
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true); // 获取的信息以文件流的形式返回
        curl_setopt($this->curl, CURLOPT_ENCODING, '');
        curl_setopt($this->curl, CURLOPT_MAXREDIRS, 10);
        curl_setopt($this->curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, true);
    }

    public static function init(): Http
    {
        return new static;
    }

    public function setHeader( array $header ): Http
    {
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, $header);

        return $this;
    }

    public function setType( string $type ): Http
    {
        $this->type = $type;

        return $this;
    }

    public function setOption( array $option ): Http
    {
        $this->option = $option;

        return $this;
    }

    public function setBody( $body ): Http
    {
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, json_encode($body));

        return $this;
    }

    public function setUrl( $url ): Http
    {
        $this->url = $url;

        return $this;
    }

    public function setTimeout( $setopt ): Http
    {
        $this->timeout = $setopt;

        return $this;
    }

    public function getResponse(): string
    {
        return $this->response;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function request(): Http
    {
        $url = $this->url;
        empty($this->option) || $url .= '?' . http_build_query($this->option);
        curl_setopt($this->curl, CURLOPT_URL, $url);
        curl_setopt($this->curl, CURLOPT_TIMEOUT, $this->timeout); // 设置超时限制防止死循环
        curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, $this->type);
        $this->response   = curl_exec($this->curl); // 执行
        $this->statusCode = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);
        curl_close($this->curl);

        return $this;
    }
}