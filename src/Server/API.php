<?php

namespace EasyApple\Server;

use EasyApple\Http;
use Firebase\JWT\JWT;

class API
{
    public string    $endpoint = 'https://api.storekit-sandbox.itunes.apple.com/inApps/v1';
    protected string $privateKeyId;
    protected string $privateKey;
    protected string $publicKey;
    protected string $issuerId;

    public function __construct(array $config)
    {
        $this->privateKeyId = $config['privateKeyId'];
        $this->privateKey = $config['privateKey'];
        $this->issuerId = $config['issuerId'];
        $this->publicKey = $config['publicKey'];
    }

    /**
     * 获取token.
     */
    public function getToken(): string
    {
        $payload = [
            'iss' => $this->issuerId,
            'iat' => time(),
            'exp' => time() + 3600,
            'aud' => 'appstoreconnect-v1',
            'nonce' => md5(uniqid()),
            'bid' => 'com.zqhlzhq.com',
        ];

        return JWT::encode($payload, $this->privateKey, 'ES256', $this->privateKeyId);
    }

    /**
     * @param mixed $path
     * @param mixed $method
     * @param mixed $option
     * @param mixed $body
     */
    protected function request($path, $method = 'GET', $option = [], $body = []): string
    {
        $uri = sprintf('%s/%s', $this->endpoint, $path);
        $client = Http::init();
        $client->setHeader([
            'Authorization: Bearer '.$this->getToken(),
            'Content-Type: application/json',
        ]);
        $client->setType($method);
        $client->setUrl($uri);
        empty($option) || $client->setOption($option);
        empty($body) || $client->setBody($body);
        $client->request();

        return $client->getResponse();
    }
}
