<?php

namespace EasyApple\Server\History;

use EasyApple\Server\API;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class TransactionHistory extends API
{
    public string $originalTransactionId;

    public function __construct(array $config)
    {
        parent::__construct($config);
    }

    public function setTransactionId($originalTransactionId): static
    {
        $this->originalTransactionId = $originalTransactionId;

        return $this;
    }

    public function get()
    {
        $data = $this->request('history/'.$this->originalTransactionId);
        $data = json_decode($data, true);
        foreach ($data['signedTransactions'] as &$v) {
            $v = (array) JWT::decode($v, new Key($this->publicKey, 'ES256'));
        }

        return $data;
    }
}
