<?php

namespace EasyApple\Server\History;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['transactionHistory'] = function ($app) {
            return new TransactionHistory($app->getConfig());
        };
    }
}
