<?php

namespace EasyApple\Server;

use EasyApple\ServiceContainer;

/**
 * Class Application.
 *
 * @property History\TransactionHistory $transactionHistory
 */
class Application extends ServiceContainer
{
    protected array $providers = [
        History\ServiceProvider::class,
    ];
}
