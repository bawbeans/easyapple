<?php

namespace EasyApple\Connect\Report;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['salesTrends'] = function ($app) {
            return new SalesTrends($app->getConfig());
        };
    }
}