<?php

namespace EasyApple\Connect\Report;

use EasyApple\Connect\API;

class SalesTrends extends API
{
    public string    $date;
    protected string $vendorNumber = '90405887';
    protected string $frequency = 'DAILY';
    protected string $subType = 'DETAILED';
    protected string $type = 'SALES';
    protected string $version = '1_3';

    public function __construct(array $config)
    {
        parent::__construct($config);
        $this->date = date('Y-m-d');
    }

    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @param mixed $frequency
     */
    public function setFrequency($frequency)
    {
        if (!in_array($frequency, ['DAILY', 'WEEKLY', 'MONTHLY', 'YEARLY'])) {
            throw new \Exception('frequency 格式不正确');
        }
        $this->frequency = $frequency;

        return $this;
    }

    /**
     * @param mixed $type
     *
     * @throws \Exception
     */
    public function setType($type)
    {
        if (!in_array($type, ['SALES', 'PRE_ORDER', 'NEWSSTAND', 'SUBSCRIPTION', 'SUBSCRIPTION_EVENT', 'SUBSCRIBER'])) {
            throw new \Exception('type 格式不正确');
        }
        $this->type = $type;

        return $this;
    }

    /**
     * @param mixed $subType
     *
     * @throws \Exception
     */
    public function setSubType($subType)
    {
        if (!in_array($subType, ['SUMMARY', 'DETAILED'])) {
            throw new \Exception('subType 格式不正确');
        }

        $this->subType = $subType;

        return $this;
    }

    public function setVendorNumber($vendorNumber)
    {
        $this->vendorNumber = $vendorNumber;

        return $this;
    }

    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    public function getVersion(): string
    {
        if (in_array($this->type, ['SUBSCRIPTION', 'SUBSCRIPTION_EVENT', 'SUBSCRIBER']) && 'DAILY' == $this->frequency) {
            return in_array($this->version, ['1_2', '1_3']) ? $this->version : '1_3';
        }

        return '1_0';
    }

    public function get(): array
    {
        return $this->request('salesReports', 'GET', [
            'filter[frequency]' => $this->frequency,
            'filter[reportDate]' => $this->date,
            'filter[reportSubType]' => $this->subType,
            'filter[reportType]' => $this->type,
            'filter[vendorNumber]' => $this->vendorNumber,
            'filter[version]' => $this->getVersion(),
        ]);
    }

    public function sales()
    {
        return $this->setType('SALES')->get();
    }

    public function pre_order()
    {
        return $this->setType('PRE_ORDER')->get();
    }

    public function newsstand()
    {
        return $this->setType('NEWSSTAND')->get();
    }

    public function subscription()
    {
        return $this->setType('SUBSCRIPTION')->get();
    }

    public function subscription_event()
    {
        return $this->setType('SUBSCRIPTION_EVENT')->get();
    }

    public function subscriber()
    {
        return $this->setType('SUBSCRIBER')->get();
    }
}
