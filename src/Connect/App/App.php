<?php

namespace EasyApple\Connect\App;

use EasyApple\Connect\API;

class App extends API
{
    /**
     *
     */
    public function getApp(): array
    {
        return $this->request('apps');
    }
}
