<?php

namespace EasyApple\Connect\App;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * Class ServiceProvider.
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['app'] = function ($app) {
            return new App($app->getConfig());
        };
    }
}
