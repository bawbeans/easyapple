<?php

namespace EasyApple\Connect;

use Firebase\JWT\JWT;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;

class API
{
    public string    $endpoint = 'https://api.appstoreconnect.apple.com/v1/';
    protected string $privateKeyId;
    protected string $privateKey;
    protected string $issuerId;

    public function __construct(array $config)
    {
        $this->privateKeyId = $config['privateKeyId'];
        $this->privateKey = $config['privateKey'];
        $this->issuerId = $config['issuerId'];
    }

    /**
     * 获取token.
     */
    public function getToken(): string
    {
        $payload = [
            'iss' => $this->issuerId,
            'exp' => time() + 20 * 60,
            'aud' => 'appstoreconnect-v1',
        ];

        return JWT::encode($payload, $this->privateKey, 'ES256', $this->privateKeyId);
    }

    public function getFileNameGz(): string
    {
        $hash = 'F-';
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $newChars = str_split($chars);
        shuffle($newChars);
        $charsKey = array_rand($newChars, 15);
        $fnstr = '';
        for ($i = 0; $i < 15; ++$i) {
            $fnstr .= $newChars[$charsKey[$i]];
        }

        return $hash.md5($fnstr.time().microtime(true) * 1000000);
    }

    /**
     * @param mixed $path
     * @param mixed $method
     * @param mixed $option
     * @param mixed $body
     *
     * @throws GuzzleException
     */
    protected function request($path, $method = 'GET', $option = [], $body = []): array
    {
        $client = new Client(['base_uri' => $this->endpoint, 'verify' => false]);
        $fileName = $this->getFileNameGz();
        $path .= '?'.http_build_query($option);

        try {
            $client->request($method, $path, [
                'decode_content' => false,
                'headers' => [
                    'Authorization' => 'Bearer '.$this->getToken(),
                    'Accept' => 'application/a-gzip',
                ],
                RequestOptions::SINK => $fileName,
            ]);
        } catch (GuzzleException $e) {
            unlink($fileName);

            return [];
        }
        $buffer_size = 4096;
        $file = gzopen($fileName, 'rb');
        $report = '';
        while (!gzeof($file)) {
            $report .= gzread($file, $buffer_size);
        }
        gzclose($file);
        $data = explode(PHP_EOL, $report);
        $data = array_filter($data);
        foreach ($data as &$v) {
            $v = explode("\t", $v);
        }
        unset($v);
        $key = [];
        $t = array_shift($data);
        foreach ($t as $v) {
            $key[] = str_replace(' ', '_', strtolower($v));
        }
        unset($v);
        $rows = [];
        foreach ($data as $v) {
            $rows[] = array_combine($key, str_replace([PHP_EOL, ' '], '', $v));
        }

        unlink($fileName);

        return $rows;
    }
}
