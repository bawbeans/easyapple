<?php

namespace EasyApple\Connect;

use EasyApple\ServiceContainer;

/**
 * Class Application.
 *
 * @property Report\SalesTrends $salesTrends
 * @property App\app            $app
 */
class Application extends ServiceContainer
{
    protected array $providers = [
        Report\ServiceProvider::class,
        App\ServiceProvider::class,
    ];
}
